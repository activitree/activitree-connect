import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.scss']
})
export class CreateProfileComponent implements OnInit {

  profileForm: FormGroup;
  schoolForm: FormGroup;
  subjects = new FormControl();
  subjectList: string[] = [
    "Biology",
    "Chemistry",
    "Physics",
    "Geography",
    "Civics",
    "History",
    "Economics",
    "Computer Science",
    "Mathematics",
    "English"
  ];
  interests = new FormControl();
  interestList: string[] = [
    "Art",
    "Music",
    "Cooking",
    "Video games",
    "Sewing",
    "Biking",
    "Hiking"
  ];

  selectedSubjects: string[] = [];
  selectedInterests: string[] = [];

  constructor(private formBuilder: FormBuilder, public auth: AngularFireAuth, private service: UserService) { }

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      username: ['', Validators.required],
      bio: [''],
    });

    this.schoolForm = this.formBuilder.group({
      school: ['', Validators.required],
      year: ['', Validators.required],
      subjects: ['', Validators.required],
      role: ['', Validators.required]
    })

    this.auth.user.subscribe(user => {
      this.profileForm.patchValue({
        name: user.displayName,
        email: user.email
      });
    });
  }

  updateName(): void {
    this.auth.user.subscribe(user =>
      user.updateProfile({
        displayName: this.profileForm.value.name
      })
    );
  }

  submit(): void {
    this.auth.user.subscribe(user =>
      this.service.set({
        name: this.profileForm.value.name,
        id: user.uid,
        username: this.profileForm.value.username,
        school: this.schoolForm.value.school,
        year: this.schoolForm.value.year,
        role: this.schoolForm.value.role,
        majors: this.subjects.value,
        interests: this.interests.value,
        bio: this.profileForm.value.bio,
      })
    );
  }

}
